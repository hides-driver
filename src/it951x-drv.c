#include "it951x-core.h"

#include "IT9510Firmware.h"

#include "firmware_V2.h"
#include "firmware_V2I.h"
#include "IQ_fixed_table.h"

#define RX_LNA_TUNER_ID_SUPPORT_TYPE	OMEGA_LNA_Config_5	// RX Decryption. Set tuner id for 0x65
#define FW_VER         					0x08060000
#define TURN_OFF_UNUSED_OLD_POWER_CTRL


int dvb_usb_it951x_debug;
module_param_named(debug,dvb_usb_it951x_debug, int, 0644);

static DEFINE_MUTEX(it951x_mutex);

static DWORD DRV_NIMReset(
	void* handle);

static DWORD DRV_InitNIMSuspendRegs(
	void* handle);
	
DWORD DRV_TunerSuspend(
	void * handle,
	BYTE ucChip,
	bool bOn);

static DWORD DRV_NIMReset(
	void* handle);

#ifndef TURN_OFF_UNUSED_OLD_POWER_CTRL
static DWORD DL_NIMReset(
	void* handle);
#endif

static DWORD DRV_InitNIMSuspendRegs(
	void* handle);

#ifndef TURN_OFF_UNUSED_OLD_POWER_CTRL
static DWORD DL_InitNIMSuspendRegs(
	void* handle);
#endif

static DWORD DRV_Initialize(
	void* handle);

static DWORD DL_Initialize(
	void* handle);
  
//************** DRV_ *************//
static DWORD DRV_IrTblDownload(IN void* handle)
{
        DWORD dwError = Error_NO_ERROR;
        PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT)handle;
        struct file *filp;
        unsigned char b_buf[512] ;
        int fileSize;
        mm_segment_t oldfs;

        deb_data("- Enter %s Function -\n",__FUNCTION__);

        oldfs=get_fs();
        set_fs(KERNEL_DS);

        filp=filp_open("/lib/firmware/af35irtbl.bin", O_RDWR,0644);
        if ( IS_ERR(filp) ) {
                deb_data("      LoadIrTable : Can't open file\n");goto exit;}

        if ( (filp->f_op) == NULL ) {
                deb_data("      LoadIrTable : File Operation Method Error!!\n");goto exit;}

        filp->f_pos=0x00;
        fileSize = filp->f_op->read(filp,b_buf,sizeof(b_buf),&filp->f_pos);

        dwError = IT9510_loadIrTable((IT9510INFO*) &pdc->modulator, (Word)fileSize, b_buf);
        if (dwError) {deb_data("Modulator_loadIrTable fail"); goto exit;}

        filp_close(filp, NULL);
        set_fs(oldfs);

exit:
		deb_data("LoadIrTable fail!\n");
        return (dwError);


}

static DWORD DRV_getFirmwareVersionFromFile( 
		void* handle,
	 	Processor	processor, 
		DWORD* 		version
)
{

	
	PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT)handle;
	BYTE chip_version = 0;
	DWORD chip_Type;
	BYTE var[2];
	DWORD error = Error_NO_ERROR;

	DWORD OFDM_VER1;
    DWORD OFDM_VER2;
    DWORD OFDM_VER3;
    DWORD OFDM_VER4;

    DWORD LINK_VER1;
    DWORD LINK_VER2;
    DWORD LINK_VER3;    
    DWORD LINK_VER4;    
    

	error = IT9510_readRegister((IT9510INFO*) &pdc->modulator, processor, chip_version_7_0, &chip_version);
	error = IT9510_readRegisters((IT9510INFO*) &pdc->modulator, processor, chip_version_7_0+1, 2, var);
	
	if(error) deb_data("DRV_getFirmwareVersionFromFile fail");
	
	chip_Type = var[1]<<8 | var[0];	
	if(chip_Type == 0x9135 && chip_version == 2){
		OFDM_VER1 = DVB_V2_OFDM_VERSION1;
		OFDM_VER2 = DVB_V2_OFDM_VERSION2;
		OFDM_VER3 = DVB_V2_OFDM_VERSION3;
		OFDM_VER4 = DVB_V2_OFDM_VERSION4;

		LINK_VER1 = DVB_V2_LL_VERSION1;
		LINK_VER2 = DVB_V2_LL_VERSION2;
		LINK_VER3 = DVB_V2_LL_VERSION3;    
		LINK_VER4 = DVB_V2_LL_VERSION4;
	}else{
		OFDM_VER1 = IT9510_DVB_OFDM_VERSION1;
    	OFDM_VER2 = IT9510_DVB_OFDM_VERSION2;
   	 	OFDM_VER3 = IT9510_DVB_OFDM_VERSION3;
    	OFDM_VER4 = IT9510_DVB_OFDM_VERSION4;

   		LINK_VER1 = IT9510_DVB_LL_VERSION1;
    	LINK_VER2 = IT9510_DVB_LL_VERSION2;
    	LINK_VER3 = IT9510_DVB_LL_VERSION3;    
    	LINK_VER4 = IT9510_DVB_LL_VERSION4;
	}

    if(processor == Processor_OFDM) {
        *version = (DWORD)( (OFDM_VER1 << 24) + (OFDM_VER2 << 16) + (OFDM_VER3 << 8) + OFDM_VER4);
    }
    else { //LINK
        *version = (DWORD)( (LINK_VER1 << 24) + (LINK_VER2 << 16) + (LINK_VER3 << 8) + LINK_VER4);    
    }
    
    return *version;
}

DWORD DRV_getDeviceType(void *handle)
{
	DWORD dwError = Error_NO_ERROR;
   	PDEVICE_CONTEXT PDC = (PDEVICE_CONTEXT) handle;

	dwError =  IT9510User_getDeviceType((IT9510INFO*) &PDC->modulator, &PDC->deviceType);
 
    return(dwError);
}

static DWORD  DRV_Initialize(
	    void *      handle
)
{
	DWORD error = Error_NO_ERROR;
	PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT)handle;
	Byte temp = 0;
	DWORD fileVersion, cmdVersion = 0; 
	SystemConfig syscfg;

	deb_data("- Enter %s Function -\n",__FUNCTION__);

	/* Get & Set Device Type */
	error = IT9510User_getDeviceType((IT9510INFO*) &pdc->modulator, &pdc->deviceType);
	if(error != Error_NO_ERROR) {
		pdc->deviceType = IT9510User_DEVICETYPE;
		pdc->isUsingDefaultDeviceType = true;
		deb_data("IT9510User_getDeviceType\n\t- No EEPROM or EEPROM Read no Value.\n");
		deb_data("\t- Load Device Type: 0x%X from default\n", pdc->deviceType);
	} else {
		pdc->isUsingDefaultDeviceType = false;		
		deb_data("IT9510User_getDeviceType\n\t- Device Type: 0x%X load from EEPROM\n", pdc->deviceType);
	}
	if(IT9510User_getDeviceTypeSetting((IT9510INFO*) &pdc->modulator, pdc->deviceType, &syscfg) != 0)
		deb_data("- IT9510User_getDeviceTypeSetting fail -\n");	
 	pdc->modulator.systemConfig = syscfg;
	if(IT9510User_setSystemConfig((IT9510INFO*) &pdc->modulator, syscfg) != 0)
		deb_data("- EagleUser_setSystemConfig fail -\n");	

	/* Read RX I2C Address from eeprom */
	switch(pdc->deviceType) {
		case 0x9C:
			printk("Device is IT951X+RFFC2072 serial\n");
			break;
		case 0xD0:			/* ADRF6755. Slave IIC Address had determined. No Need to Read IIC Slave Address by EEPROM */ 
		case 0xD1:
		case 0xD2:
		case 0xD3:
		case 0xD4:
		case 0xD5:
		case 0xD6:
		case 0xD7:
		case 0xD8:
		case 0xD9:
			printk("Device is IT951X+ADRF6755 40Mhz serial\n");
			break;		
		case 0xDA:
			printk("Device is IT951X+ADRF6755 20Mhz serial\n");
			break;		
		case 0xDB:
			printk("Device is IT951X+ADRF6755+Orion 20Mhz serial\n");
			break;		
		case 0xDC:
		case 0xDD:
			printk("Device is IT951X+ADRF6755+Orion 20Mhz serial. ID: IT9518-DB-04-01-V03\n");
			break;
		case 0xDE:
		case 0xDF:
			printk("Device is IT951X+ADRF6755 serial\n");
			break;
			
		default:		/* RX device */
			error = IT9510_readRegister ((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x4979, &temp);//has eeprom ??
			if((temp == 1) && (error == ModulatorError_NO_ERROR)){  // eeprom
				/* Load RX I2C Address From EEPROM */
				error = IT9510_readRegister ((IT9510INFO*) &pdc->modulator, Processor_LINK, EEPROM_SLAVEI2CADDRESS, &temp);
				if(error == ModulatorError_NO_ERROR) {
					deb_data("RX SlaveII address: %X\n", temp);
				} else { 
					deb_data("RX SlaveII load address fail: %X\n", temp);		
					deb_data("\t- Set as default: %X\n", SLAVE_DEMOD_2WIREADDR);
					temp = SLAVE_DEMOD_2WIREADDR;
				}
					
				if(IT9510_setSlaveIICAddress((IT9510INFO*) &pdc->modulator, temp) != 0)
					deb_data("- IT9510_setSlaveIICAddress fail -\n");	
				else
					deb_data("- IT9510_setSlaveIICAddress:%X ok -\n", temp);
			} else {    // no eeprom
				/* Read Default */
				if(IT9510_setSlaveIICAddress((IT9510INFO*) &pdc->modulator, SLAVE_DEMOD_2WIREADDR) != 0)
					deb_data("- IT9510_setSlaveIICAddress fail -\n");	
				else
					deb_data("- IT9510_setSlaveIICAddress as default: %X ok -\n", SLAVE_DEMOD_2WIREADDR);	
			}
		break;
	}

	if(pdc->modulator.booted) {//from Standard_setBusTuner() > Standard_getFirmwareVersion()
        	//use "#define version" to get fw version (from firmware.h title)
        	error = DRV_getFirmwareVersionFromFile(handle, Processor_OFDM, &fileVersion);

        	//use "Command_QUERYINFO" to get fw version 
        	error = IT9510_getFirmwareVersion((IT9510INFO*) &pdc->modulator, Processor_OFDM, &cmdVersion);
        	if(error) deb_data("DRV_Initialize : IT9510_getFirmwareVersion : error = 0x%lx\n", error);
        	if(cmdVersion != fileVersion) {
				deb_data("Reboot: Outside Fw = 0x%lx, Inside Fw = 0x%lx", fileVersion, cmdVersion);
				error = IT9510_TXreboot((IT9510INFO*) &pdc->modulator);
				pdc->bBootCode = true;
				if(error) {
					deb_data("IT9510_reboot : error = 0x%08ld\n", error);
					return error;
				} else {
					return Error_NOT_READY;
				}
        	} else {
            		deb_data("	Fw version is the same!\n");
  	      		error = Error_NO_ERROR;
        	}
	}//pdc->IT951x.booted
	
	error = IT9510_readRegister ((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x4979, &temp);//has eeprom ?
	if (error) deb_data("Eagle_readRegister: 0x%08ld\n", error);

    if (temp == 1) { // has eeprom
		/* Read Stream Type */
		error = IT9510_readRegister ((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x49CA, &temp);
		if (error) deb_data("Eagle_readRegister: 0x%08ld\n", error);

		switch(temp) {	// StreamType
			case StreamType_NONE:
			{
				deb_data("    TS_Interface_Normal.StreamType_DVBT_SERIAL\n");
				/* TX */
				error = IT9510_initialize ((IT9510INFO*) &pdc->modulator , StreamType_DVBT_SERIAL, 2, 0);
				if (error) {
					pdc->Tx_init_success = 0;
					deb_data("    Device initialize TX: NO. ErrorCode [0x%lx]\n", error);
					deb_data("    Device initialize TX Handler: NO\n");							
				} else {
					pdc->Tx_init_success = 1;
					deb_data("    Device initialize TX: YES\n");
					deb_data("    Device initialize TX Handler: YES\n");					
				}

				break;
			}			
			case StreamType_DVBT_DATAGRAM:
			{
				deb_data("    TS_Interface_IT95X7_only.StreamType_DVBT_DATAGRAM\n");
				/* TX */
				error = IT9510_initialize ((IT9510INFO*) &pdc->modulator , StreamType_DVBT_DATAGRAM, 2, 0);
				if (error) {
					pdc->Tx_init_success = 0;
					deb_data("    Device initialize TX: NO. ErrorCode [0x%lx]\n", error);
					deb_data("    Device initialize TX Handler: NO\n");							
				} else {
					pdc->Tx_init_success = 1;
					deb_data("    Device initialize TX: YES\n");
					deb_data("    Device initialize TX Handler: YES\n");					
				}

				break;
			}
			case StreamType_DVBT_PARALLEL:
			{
				deb_data("    TS_Interface_Parallel.StreamType_DVBT_PARALLEL\n");

				/* TX */
				error = IT9510_initialize ((IT9510INFO*) &pdc->modulator , StreamType_DVBT_PARALLEL, Bus_USB, IT9510User_DEVICETYPE);			
				if (error) {
					pdc->Tx_init_success = 0;
					deb_data("    Device initialize TX: NO. ErrorCode [0x%lx]\n", error);
					deb_data("    Device initialize TX Handler: NO\n");							
				} else {
					pdc->Tx_init_success = 1;
					deb_data("    Device initialize TX: YES\n");
					deb_data("    Device initialize TX Handler: YES\n");					
				}

				break;
			}
			case StreamType_DVBT_SERIAL:
			{
				deb_data("    TS_Interface_Serial.StreamType_DVBT_SERIAL\n"); 
				
				/* TX */
				error = IT9510_initialize ((IT9510INFO*) &pdc->modulator , StreamType_DVBT_SERIAL, Bus_USB, IT9510User_DEVICETYPE);
				if (error) {
					pdc->Tx_init_success = 0;
					deb_data("    Device initialize TX: NO. ErrorCode [0x%lx]\n", error);
					deb_data("    Device initialize TX Handler: NO\n");							
				} else {
					pdc->Tx_init_success = 1;
					deb_data("    Device initialize TX: YES\n");
					deb_data("    Device initialize TX Handler: YES\n");					
				}

				break;
			}
			case TS_INTERFACE_RX_ONLY_PARALLEL:
			{
				deb_data("    TS_INTERFACE_RX_ONLY_PARALLEL.StreamType_DVBT_PARALLEL\n"); 
				
				/* TX */
				error = IT9510_initialize ((IT9510INFO*) &pdc->modulator , StreamType_DVBT_PARALLEL, Bus_USB, IT9510User_DEVICETYPE);
				if (error) {
					pdc->Tx_init_success = 0;
					deb_data("    Device initialize TX: NO. ErrorCode [0x%lx]\n", error);
					deb_data("    Device initialize TX Handler: NO\n");							
				} else {
					pdc->Tx_init_success = 0;
					deb_data("    Device initialize TX: YES\n");
					deb_data("    Device initialize TX Handler: NO\n");					
				}

				break;
			}
			case TS_INTERFACE_RX_ONLY_SERIAL:
			{
				deb_data("    TS_INTERFACE_RX_ONLY_SERIAL.StreamType_DVBT_Serial\n"); 
				
				/* TX */
				error = IT9510_initialize ((IT9510INFO*) &pdc->modulator , StreamType_DVBT_SERIAL, Bus_USB, IT9510User_DEVICETYPE);
				if (error) {
					pdc->Tx_init_success = 0;
					deb_data("    Device initialize TX: NO. ErrorCode [0x%lx]\n", error);
					deb_data("    Device initialize TX Handler: NO\n");							
				} else {
					pdc->Tx_init_success = 0;
					deb_data("    Device initialize TX: YES\n");
					deb_data("    Device initialize TX Handler: NO\n");					
				}

				break;
			}
			default:
			{
				deb_data("    UnKnown Stream Type! Use Default\n");
				deb_data("    StreamType_DVBT_SERIAL\n");
				/* TX */
				error = IT9510_initialize ((IT9510INFO*) &pdc->modulator , StreamType_DVBT_SERIAL, 2, 0);
				if (error) {
					pdc->Tx_init_success = 0;
					deb_data("    Device initialize TX: NO. ErrorCode [0x%lx]\n", error);
					deb_data("    Device initialize TX Handler: NO\n");							
				} else {
					pdc->Tx_init_success = 1;
					deb_data("    Device initialize TX: YES\n");
					deb_data("    Device initialize TX Handler: YES\n");					
				}

				break;
			}
		} 
	} else {
		deb_data("  No Detected EEPROM: Default TS_Interface_Normal.StreamType_DVBT_SERIAL\n"); 
		/* TX */
		error = IT9510_initialize ((IT9510INFO*) &pdc->modulator , StreamType_DVBT_SERIAL, Bus_USB, IT9510User_DEVICETYPE);
		if (error) {
			pdc->Tx_init_success = 0;
			deb_data("    Device initialize TX: NO. ErrorCode [0x%lx]\n", error);
			deb_data("    Device initialize TX Handler: NO\n");							
		} else {
			pdc->Tx_init_success = 1;
			deb_data("    Device initialize TX: YES\n");
			deb_data("    Device initialize TX Handler: YES\n");					
		}

	}
	
    IT9510_getFirmwareVersion ((IT9510INFO*) &pdc->modulator, Processor_OFDM, &cmdVersion);
    deb_data("    FwVer OFDM = 0x%lx, ", cmdVersion);
    IT9510_getFirmwareVersion ((IT9510INFO*) &pdc->modulator, Processor_LINK, &cmdVersion);
    deb_data("    FwVer LINK = 0x%lx\n", cmdVersion);
    
    
    return error;
	
}

static DWORD DRV_InitDevInfo(
    	void *      handle,
    	BYTE        ucSlaveDemod
)
{
    DWORD dwError = Error_NO_ERROR;    
   	PDEVICE_CONTEXT PDC = (PDEVICE_CONTEXT) handle;
    PDC->fc[ucSlaveDemod].ulCurrentFrequency = 0;  
    PDC->fc[ucSlaveDemod].ucCurrentBandWidth = 0;

    PDC->fc[ucSlaveDemod].ulDesiredFrequency = 0;	
    PDC->fc[ucSlaveDemod].ucDesiredBandWidth = 6000;	

    //For PID Filter Setting
    PDC->fc[ucSlaveDemod].bEnPID = false;

    PDC->fc[ucSlaveDemod].bApOn = false;
    
    PDC->fc[ucSlaveDemod].bResetTs = false;



    PDC->fc[ucSlaveDemod].tunerinfo.bTunerOK = false;
    PDC->fc[ucSlaveDemod].tunerinfo.bSettingFreq = false;

    return dwError;
}	

//get EEPROM_IRMODE/bIrTblDownload/bRAWIr/architecture config from EEPROM
static DWORD DRV_GetEEPROMConfig(
	void* handle)
{       
    DWORD dwError = Error_NO_ERROR;
	BYTE chip_version = 0;
	DWORD chip_Type;
	BYTE  var[2];
    PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT)handle;
	//bIrTblDownload option
    Byte btmp = 0;
	int ucSlaveDemod;
	
	deb_data("- Enter %s Function -",__FUNCTION__);

	//patch for read eeprom valid bit
	dwError = IT9510_readRegister((IT9510INFO*) &pdc->modulator, Processor_LINK, chip_version_7_0, &chip_version);
	dwError = IT9510_readRegisters((IT9510INFO*) &pdc->modulator, Processor_LINK, chip_version_7_0+1, 2, var);

	if(dwError) deb_data("DRV_GetEEPROMConfig fail---cant read chip version");

	chip_Type = var[1]<<8 | var[0];
	if(chip_Type==0x9135 && chip_version == 2) //Om2
	{
		pdc->chip_version = 2;
		dwError = IT9510_readRegisters((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x461d, 1, &btmp);
		deb_data("Chip Version is %d---and Read 461d---valid bit = 0x%02X", chip_version, btmp);
	}
	else 
	{
		pdc->chip_version = 1; //Om1
		dwError = IT9510_readRegisters((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x4979, 1, &btmp);
		deb_data("Chip Version is %d---and Read 4979---valid bit = 0x%02X", chip_version, btmp);
	}
	if (dwError) 
	{
		deb_data("0x461D eeprom valid bit read fail!");
		goto exit;
    }

	if(btmp == 0)
	{
		deb_data("=============No need read eeprom");
		pdc->bIrTblDownload = false;
		pdc->bProprietaryIr = false;
		pdc->bSupportSelSuspend = false;
		pdc->bDualTs = false;
    	pdc->architecture = Architecture_DCA;
    	pdc->bDCAPIP = false;
#if IT9133Rx    	
		pdc->fc[0].tunerinfo.TunerId = 0x38;
#else
		pdc->fc[0].tunerinfo.TunerId = 0x70;
#endif
	}
	else
	{
		deb_data("=============Need read eeprom");
		dwError = IT9510_readRegisters((IT9510INFO*) &pdc->modulator, Processor_LINK, EEPROM_IRMODE, 1, &btmp);
    	if (dwError) goto exit;
    	pdc->bIrTblDownload = btmp ? true:false;
    	deb_data("EEPROM_IRMODE = 0x%02X, ", btmp);	
    	deb_data("bIrTblDownload %s\n", pdc->bIrTblDownload?"ON":"OFF");
    	pdc->bProprietaryIr = (btmp==0x05)?true:false;
    	deb_data("bRAWIr %s\n", pdc->bProprietaryIr?"ON":"OFF");
		if(pdc->bProprietaryIr)
		{
			deb_data("IT951x proprietary (raw) mode\n");
		}
		else
		{
			deb_data("IT951x HID (keyboard) mode\n");
		}		    	        	    
		//EE chose NEC RC5 RC6 threshhold table
		if(pdc->bIrTblDownload)
		{
			dwError = IT9510_readRegisters((IT9510INFO*) &pdc->modulator, Processor_LINK, EEPROM_IRTYPE, 1, &btmp);
    		if (dwError) goto exit;
			pdc->bIrType = btmp;
			deb_data("bIrType 0x%02X", pdc->bIrType);
		}
//selective suspend
    	pdc->bSupportSelSuspend = false;
    	deb_data("SelectiveSuspend = %s", pdc->bSupportSelSuspend?"ON":"OFF");
    	    
//bDualTs option   
    	pdc->bDualTs = false;
    	pdc->architecture = Architecture_DCA;
    	pdc->bDCAPIP = false;

    	dwError = IT9510_readRegisters((IT9510INFO*) &pdc->modulator, Processor_LINK, EEPROM_TSMODE, 1, &btmp);
    	if (dwError) goto exit;
    	deb_data("EEPROM_TSMODE = 0x%02X", btmp);

    	if (btmp == 0)     
    	{  
        	deb_data("TSMode = TS1 mode\n");
    	}
    	else if (btmp == 1) 
   		{
        	deb_data("TSMode = DCA+PIP mode\n");
			pdc->architecture = Architecture_DCA;
        	pdc->bDualTs = true;
        	pdc->bDCAPIP = true;
    	}
    	else if (btmp == 2) 
    	{ 
        	deb_data("TSMode = DCA mode\n");
    	}
    	else if (btmp == 3) 
    	{
        	deb_data("TSMode = PIP mode\n");
        	pdc->architecture = Architecture_PIP;
        	pdc->bDualTs = true;
    	}

//tunerID option, in Omega, not need to read register, just assign 0x38;
		dwError = IT9510_readRegisters((IT9510INFO*) &pdc->modulator, Processor_LINK, EEPROM_TUNERID, 1, &btmp);
		if (btmp==0x51) {
			pdc->fc[0].tunerinfo.TunerId = 0x51;  	
		}
		else if (btmp==0x52) {
			pdc->fc[0].tunerinfo.TunerId = 0x52;  	
		}
		else if (btmp==0x60) {
			pdc->fc[0].tunerinfo.TunerId = 0x60;  	
		}
		else if (btmp==0x61) {
			pdc->fc[0].tunerinfo.TunerId = 0x61;  	
		}
		else if (btmp==0x62) {
			pdc->fc[0].tunerinfo.TunerId = 0x62;  	
		}
		else {
#if IT9133Rx			
			pdc->fc[0].tunerinfo.TunerId = 0x38;  	
#else
			pdc->fc[0].tunerinfo.TunerId = 0x70;  	
#endif
		}		
	
		if (pdc->bDualTs) {
			pdc->fc[1].tunerinfo.TunerId = pdc->fc[0].tunerinfo.TunerId;
		}

		dwError = IT9510_readRegisters((IT9510INFO*) &pdc->modulator, Processor_LINK, EEPROM_SUSPEND, 1, &btmp);
		deb_data("EEPROM susped mode=%d", btmp);
    	
    }
//init some device info
	for(ucSlaveDemod = 0; ucSlaveDemod <= (BYTE)pdc->bDualTs; ucSlaveDemod++)
	{
		dwError = DRV_InitDevInfo(handle, ucSlaveDemod);
	}
	
exit:
    return(dwError);
}


DWORD NIM_ResetSeq(IN  void *	handle)
{
	DWORD dwError = Error_NO_ERROR;
	PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT)handle;
	int i;
	
	BYTE bootbuffer[6];
	bootbuffer[0] = 0x05;
	bootbuffer[1] = 0x00;
	bootbuffer[2] = 0x23;
	bootbuffer[3] = 0x01;
	bootbuffer[4] = 0xFE;
	bootbuffer[5] = 0xDC;	

	//aaa
	//reset 9133 -> boot -> demod init

	//GPIOH5 init
	dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK, p_reg_top_gpioh5_en, reg_top_gpioh5_en_pos, reg_top_gpioh5_en_len, 0);
	dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK, p_reg_top_gpioh5_on, reg_top_gpioh5_on_pos, reg_top_gpioh5_on_len, 0);
		

	deb_data("aaa start DRV_NIMReset");
	dwError = DRV_NIMReset(handle);

	dwError = DRV_InitNIMSuspendRegs(handle);
	
	deb_data("aaa start writeGenericRegisters");
	dwError = IT9510_writeGenericRegisters ((IT9510INFO*) &pdc->modulator, 0x3a, 0x06, bootbuffer);
	
	deb_data("aaa start readGenericRegisters");
	dwError = IT9510_readGenericRegisters ((IT9510INFO*) &pdc->modulator, 0x3a, 0x05, bootbuffer);
	deb_data("aaa print I2C reply");
	for(i=0; i<5; i++)
		deb_data("aaa bootbuffer[%d] = 0x%x", i, bootbuffer[i]);
	
	msleep(50); //delay for Fw boot	

	
	//Demod & Tuner init
	deb_data("aaa DL_Initialize");
	dwError = DRV_Initialize(handle);

	return dwError;
}


static DWORD DRV_Reboot(
      void *     handle
)
{
	DWORD dwError = Error_NO_ERROR;

        PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT) handle;

        deb_data("- Enter %s Function -\n",__FUNCTION__);

        dwError = IT9510_TXreboot((IT9510INFO*) &pdc->modulator);

	return(dwError);
}


#ifndef TURN_OFF_UNUSED_OLD_POWER_CTRL
static DWORD DRV_USBSetup(
    void*	handle
)
{
    DWORD dwError = Error_NO_ERROR;

    PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT) handle;
    int i;

    deb_data("- Enter %s Function -\n",__FUNCTION__);



    return(dwError);
}
#endif

static DWORD DRV_InitNIMSuspendRegs(
    void *      handle
)
{
    DWORD dwError = Error_NO_ERROR;

    PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT) handle;
    deb_data("- Enter %s Function -\n",__FUNCTION__);

    dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK, p_reg_top_gpioh5_en, reg_top_gpioh5_en_pos, reg_top_gpioh5_en_len, 1);
    dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK, p_reg_top_gpioh5_on, reg_top_gpioh5_on_pos, reg_top_gpioh5_on_len, 1);
    dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK, p_reg_top_gpioh5_o, reg_top_gpioh5_o_pos, reg_top_gpioh5_o_len, 0);

    dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK, p_reg_top_pwrdw, reg_top_pwrdw_pos, reg_top_pwrdw_len, 1);

    dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK, p_reg_top_pwrdw_hwen, reg_top_pwrdw_hwen_pos, reg_top_pwrdw_hwen_len, 1);

    return(dwError);
}

static DWORD DRV_NIMReset(
    void *      handle
)
{


    DWORD   dwError = Error_NO_ERROR;

    PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT)handle;
    deb_data("- Enter %s Function -\n",__FUNCTION__);
    //Set AF0350 GPIOH1 to 0 to reset AF0351

    dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK,  p_reg_top_gpioh1_en, reg_top_gpioh1_en_pos, reg_top_gpioh1_en_len, 1);
    dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK,  p_reg_top_gpioh1_on, reg_top_gpioh1_on_pos, reg_top_gpioh1_on_len, 1);
    dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK,  p_reg_top_gpioh1_o, reg_top_gpioh1_o_pos, reg_top_gpioh1_o_len, 0);

    msleep(50);

    dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK,  p_reg_top_gpioh1_o, reg_top_gpioh1_o_pos, reg_top_gpioh1_o_len, 1);

    return(dwError);
}



DWORD DRV_ResetBuffer(
	void *handle
)
{
	DWORD result = Error_NO_ERROR;
	return result;
}

DWORD DRV_DetectLockSignal(
	void *handle
)
{
	DWORD error = Error_NO_ERROR;
	Byte result;
	IT9510INFO* modulator = (IT9510INFO*)handle;
	
	// GPIO 3
	error = IT9510_readRegister ((IT9510INFO*) modulator, Processor_LINK, 0xD8B2, &result);	// Read relock signal
	if(error){
		deb_data("\t Error: Read GPIO 3 fail [0x%08lx]\n", error);
		return 0;
	}

	return result;
}


Dword DRV_SetDCCalibrationTable(void* handle) {
	PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT)handle;	
	Dword error = ModulatorError_NO_ERROR;
	Byte DCvalue[12];
	Byte OFS_I_value[5];
	Byte OFS_Q_value[5];
	Byte eeprom = 0;
	Byte DCcalibration = 0;
	Byte index = 0;//,i;
	
	DCtable* dc_table = pdc->dcInfo.ptrDCtable;
	DCtable* ofs_table = pdc->dcInfo.ptrOFStable;
	
	//-------------set DC Calibration table
     error = IT9510_readRegister ((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x4979, &eeprom);//has eeprom ??
     if (error) goto exit;
     error = IT9510_readRegister ((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x49D6, &DCcalibration);//has DCcalibration ??
     if (error) goto exit;
     if((eeprom ==1) && ((DCcalibration & 0x80) == 0x80)){
		error = IT9510_readRegisters ((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x49D6, 12, DCvalue);//DC calibration value
		if (error) goto exit;
		error = IT9510_readRegisters ((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x49CC, 5, OFS_I_value);//OFS_I calibration value
		if (error) goto exit;
		error = IT9510_readRegisters ((IT9510INFO*) &pdc->modulator, Processor_LINK, 0x49E2, 5, OFS_Q_value);//OFS_Q calibration value
		if (error) goto exit;
		for(index = 1; index < (pdc->dcInfo.tableGroups+1); index++) {

			if(index == 1){
				dc_table[index-1].startFrequency = 50000;
				ofs_table[index-1].startFrequency = 50000;
			} else if(index == 2){
				dc_table[index-1].startFrequency = 290000;
				ofs_table[index-1].startFrequency = 290000;
			} else if(index == 3){
				dc_table[index-1].startFrequency = 300000;
				ofs_table[index-1].startFrequency = 300000;
			} else if(index == 4){
				dc_table[index-1].startFrequency = 600000;
				ofs_table[index-1].startFrequency = 600000;
			} else {
				dc_table[index-1].startFrequency = 650000;
				ofs_table[index-1].startFrequency = 650000;
			}
			if((DCvalue[0] >> (index-1) ) &0x01)
				dc_table[index-1].i = DCvalue[(index*2)];
			else
				dc_table[index-1].i = DCvalue[(index*2)]*-1;
			if((DCvalue[1] >> (index-1) ) &0x01)
				dc_table[index-1].q = DCvalue[1 + (index*2)];
			else
				dc_table[index-1].q = DCvalue[1 + (index*2)]*-1;

			ofs_table[index-1].i = OFS_I_value[index-1];
			ofs_table[index-1].q = OFS_Q_value[index-1];
		}
		if(IT9510_setDCtable((IT9510INFO*) &pdc->modulator, pdc->dcInfo) == 0)
			deb_data("- SetDCCalibrationtable ok -\n");
		else
			deb_data("- SetDCCalibrationtable fail -\n");
     } else {
		deb_data("- SetDCCalibrationTable no setted -\n");
		deb_data("\t no eeprom or DC value is invalid\n");
	 }
exit:
     return error;
}

//************** DL_ *************//
#ifndef TURN_OFF_UNUSED_OLD_POWER_CTRL
static DWORD DL_NIMReset(
    void *      handle
)
{
	DWORD dwError = Error_NO_ERROR;

    mutex_lock(&it951x_mutex);
    
    dwError = DRV_NIMReset(handle);

    mutex_unlock(&it951x_mutex);

    return (dwError);
}
#endif

#ifndef TURN_OFF_UNUSED_OLD_POWER_CTRL
static DWORD DL_USBSetup(
    void *      handle
)
{
	DWORD dwError = Error_NO_ERROR;

	mutex_lock(&it951x_mutex);

	dwError = DRV_USBSetup(handle);

	mutex_unlock(&it951x_mutex);

    return (dwError);
}
#endif


#ifndef TURN_OFF_UNUSED_OLD_POWER_CTRL
static DWORD DL_InitNIMSuspendRegs(
    void *      handle
)
{
	DWORD dwError = Error_NO_ERROR;
    
	mutex_lock(&it951x_mutex);

    dwError = DRV_InitNIMSuspendRegs(handle);

    mutex_unlock(&it951x_mutex);

    return (dwError);
}
#endif

static DWORD DL_Initialize(
	    void *      handle
)
{
	DWORD dwError = Error_NO_ERROR;
    mutex_lock(&it951x_mutex);

    dwError = DRV_Initialize(handle);

	mutex_unlock(&it951x_mutex);

	return (dwError); 
    
}


static DWORD  DL_GetEEPROMConfig(
	 void *      handle
)
{   
	DWORD dwError = Error_NO_ERROR;
    mutex_lock(&it951x_mutex);

    dwError = DRV_GetEEPROMConfig(handle);

    mutex_unlock(&it951x_mutex);

    return(dwError);
} 

static DWORD  DL_IrTblDownload(
      void *     handle
)
{
	DWORD dwError = Error_NO_ERROR;

    mutex_lock(&it951x_mutex);

	dwError = DRV_IrTblDownload(handle);

    mutex_unlock(&it951x_mutex);

    return(dwError);
}


DWORD DL_ApPwCtrl (
	void* handle,
    Bool  bChipCtl,
    Bool  bOn
)
{
    DWORD dwError = Error_NO_ERROR;
	PDEVICE_CONTEXT PDC = (PDEVICE_CONTEXT)handle;
	
	mutex_lock(&it951x_mutex);

	deb_data("- Enter %s Function -",__FUNCTION__);
	deb_data("  chip =  %d  bOn = %s\n", bChipCtl, bOn?"ON":"OFF");

	if(bChipCtl) {    // IT91xx
	} else {          // IT9510
		if(bOn) {	  // resume
			deb_data("IT951x Power ON\n");				
			dwError = IT9510_controlPowerSaving ((IT9510INFO*) &PDC->modulator, bOn);				
			if(dwError) { 
				deb_data("ApCtrl::IT9510_controlPowerSaving error = 0x%04ld\n", dwError); 
				goto exit;
			}
		} else {      // suspend
			dwError = IT9510_setTxModeEnable((IT9510INFO*) &PDC->modulator, 0);
			if(dwError) {
				deb_data("ApCtrl::IT9510_setTxModeEnable error = 0x%04ld\n", dwError);
				goto exit;				
			}
			deb_data("IT951x Power OFF\n");							
			dwError = IT9510_controlPowerSaving ((IT9510INFO*) &PDC->modulator, bOn);			
			if(dwError) {
				deb_data("ApCtrl::IT9510_controlPowerSaving error = 0x%04ld\n", dwError);
				goto exit;
			}
		}			
	}

exit:
    mutex_unlock(&it951x_mutex);
    	return(dwError);
}


DWORD DL_getDeviceType(void *handle)
{
	DWORD dwError = Error_NO_ERROR;
   	PDEVICE_CONTEXT PDC = (PDEVICE_CONTEXT) handle;
	mutex_lock(&it951x_mutex);

	dwError =  DRV_getDeviceType(PDC);

	mutex_unlock(&it951x_mutex);
    return(dwError);
}

DWORD DL_ReSetInterval(void)
{
    DWORD dwError = Error_NO_ERROR;

    mutex_lock(&it951x_mutex);

    mutex_unlock(&it951x_mutex);

    return(dwError);
}

DWORD DL_Reboot(void *handle) 
{
   	PDEVICE_CONTEXT PDC = (PDEVICE_CONTEXT) handle;
	DWORD dwError = Error_NO_ERROR;
    mutex_lock(&it951x_mutex);

	deb_data("- Enter %s Function -\n",__FUNCTION__);

	dwError = DRV_Reboot(PDC);

    mutex_unlock(&it951x_mutex);

    return(dwError);
}

DWORD DL_DemodIOCTLFun(void* handle, DWORD IOCTLCode, unsigned long pIOBuffer)
{
    DWORD dwError = Error_NO_ERROR;

	mutex_lock(&it951x_mutex);



    dwError = DemodIOCTLFun(handle, IOCTLCode, pIOBuffer);


    mutex_unlock(&it951x_mutex);

    return(dwError);
}

DWORD DL_ResetBuffer(
	void* handle
)
{	
	DWORD result = Error_NO_ERROR;
	
	mutex_lock(&it951x_mutex);
		result = DRV_ResetBuffer(handle);
	mutex_unlock(&it951x_mutex);
	
	return result;
}

DWORD DL_DetectLockSignal(
	void* handle
)
{	
	DWORD result = Error_NO_ERROR;
	
	mutex_lock(&it951x_mutex);
		result = DRV_DetectLockSignal(handle);
	mutex_unlock(&it951x_mutex);
	
	return result;
}

DWORD DL_SetDCCalibrationTable(
	void* handle
)
{	
	DWORD result = Error_NO_ERROR;
	PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT)handle;	
	
	mutex_lock(&it951x_mutex);
	result = IT9510User_LoadDCCalibrationTable((IT9510INFO*) &pdc->modulator);
	mutex_unlock(&it951x_mutex);
	
	return result;
}

static DWORD DRV_TunerWakeup(
      void *     handle
)
{   
    	DWORD dwError = Error_NO_ERROR;

    	PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT) handle;

	deb_data("- Enter %s Function -\n",__FUNCTION__);

	//tuner power on
	dwError = IT9510_writeRegisterBits((IT9510INFO*) &pdc->modulator, Processor_LINK,  p_reg_top_gpioh7_o, reg_top_gpioh7_o_pos, reg_top_gpioh7_o_len, 1);

    return(dwError);

}

static DWORD DL_TunerWakeup(
      void *     handle
)
{    
	DWORD dwError = Error_NO_ERROR;
    mutex_lock(&it951x_mutex);

    dwError = DRV_TunerWakeup(handle);

    mutex_unlock(&it951x_mutex);
   
    	return(dwError);
}


static DWORD DRV_SetBusTuner(
	 void * handle, 
	 Word busId, 
	 Word tunerId
)
{
	DWORD dwError = Error_NO_ERROR;
	DWORD 	 version = 0;

	PDEVICE_CONTEXT pdc = (PDEVICE_CONTEXT)handle;


    pdc->modulator.busId = busId;    

	dwError = IT9510_getFirmwareVersion ((IT9510INFO*) &pdc->modulator, Processor_LINK, &version);
    	if (version != 0) {
        	pdc->modulator.booted = True;
    	} 
    	else {
        	pdc->modulator.booted = False;
    	}
	if (dwError) {deb_data("IT9510_getFirmwareVersion  error %lu\n", dwError);}

    	return(dwError); 
}

static DWORD DL_SetBusTuner(
	 void * handle, 
	 Word busId, 
	 Word tunerId
)
{
	DWORD dwError = Error_NO_ERROR;
	
	mutex_lock(&it951x_mutex);

    dwError = DRV_SetBusTuner(handle, busId, tunerId);

    mutex_unlock(&it951x_mutex);

	return (dwError);

}
#if 1
DWORD Device_init(struct usb_device *udev, PDEVICE_CONTEXT PDC, Bool bBoot)
{
	DWORD error = Error_NO_ERROR;
	int errcount=0;

	PDC->modulator.userData = (void *)udev;
	dev_set_drvdata(&udev->dev, PDC);

	deb_data("- Enter %s Function -\n",__FUNCTION__);

// define in it951x-core.h
#ifdef QuantaMID
	deb_data("    === AfaDTV on Quanta  ===\n");
#endif
#ifdef EEEPC
	deb_data("    === AfaDTV on EEEPC ===\n");
#endif

#ifdef DRIVER_RELEASE_VERSION
        deb_data("        DRIVER_RELEASE_VERSION  : %s\n", DRIVER_RELEASE_VERSION);
#else
	deb_data("        DRIVER_RELEASE_VERSION  : v0.0-0\n");
#endif

#ifdef __IT9510FIRMWARE_H__
	deb_data("        EAGLEII_FW_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", IT9510_DVB_LL_VERSION1, IT9510_DVB_LL_VERSION2, IT9510_DVB_LL_VERSION3, IT9510_DVB_LL_VERSION4);
	deb_data("        EAGLEII_FW_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", IT9510_DVB_OFDM_VERSION1, IT9510_DVB_OFDM_VERSION2, IT9510_DVB_OFDM_VERSION3, IT9510_DVB_OFDM_VERSION4);	
#else
	deb_data("        EAGLEII_FW_RELEASE_LINK_VERSION: v0_0_0_0\n");	
	deb_data("        EAGLEII_FW_RELEASE_OFDM_VERSION: v0_0_0_0\n");		
#endif

#ifdef __FIRMWARE_H__
	#if IT9133Rx
		deb_data("        OMEGA_FW_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", LL_VERSION1, LL_VERSION2, LL_VERSION3, LL_VERSION4);
		deb_data("        OMEGA_FW_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", OFDM_VERSION1, OFDM_VERSION2, OFDM_VERSION3, OFDM_VERSION4);	
	#else
		deb_data("        SAMBA_FW_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", DVB_LL_VERSION1, DVB_LL_VERSION2, DVB_LL_VERSION3, DVB_LL_VERSION4);
		deb_data("        SAMBA_FW_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", DVB_OFDM_VERSION1, DVB_OFDM_VERSION2, DVB_OFDM_VERSION3, DVB_OFDM_VERSION4);	
	#endif
#else
	deb_data("        RX_FW_RELEASE_LINK_VERSION: v0_0_0_0\n");	
	deb_data("        RX_FW_RELEASE_OFDM_VERSION: v0_0_0_0\n");		
#endif

#if IT9133Rx
	#ifdef __FIRMWAREV2_H__
		if(RX_LNA_TUNER_ID_SUPPORT_TYPE == OMEGA_LNA_Config_5) {		/* Support Decryption */
			deb_data("        OMEGA_FW_V2I_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", DVB_V2I_LL_VERSION1, DVB_V2I_LL_VERSION2, DVB_V2I_LL_VERSION3, DVB_V2I_LL_VERSION4);
			deb_data("        OMEGA_FW_V2I_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", DVB_V2I_OFDM_VERSION1, DVB_V2I_OFDM_VERSION2, DVB_V2I_OFDM_VERSION3, DVB_V2I_OFDM_VERSION4);				
		} else {
			deb_data("        OMEGA_FW_V2_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", DVB_V2_LL_VERSION1, DVB_V2_LL_VERSION2, DVB_V2_LL_VERSION3, DVB_V2_LL_VERSION4);
			deb_data("        OMEGA_FW_V2_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", DVB_V2_OFDM_VERSION1, DVB_V2_OFDM_VERSION2, DVB_V2_OFDM_VERSION3, DVB_V2_OFDM_VERSION4);	
		}
	#else
		deb_data("        RX_FW_V2_RELEASE_LINK_VERSION: v0_0_0_0\n");	
		deb_data("        RX_FW_V2_RELEASE_OFDM_VERSION: v0_0_0_0\n");		
	#endif
#endif

#ifdef IT9510_Version_NUMBER 
	deb_data("        API_TX_RELEASE_VERSION  : %X.%X.%X\n", IT9510_Version_NUMBER, IT9510_Version_DATE, IT9510_Version_BUILD);
#else
	deb_data("        API_TX_RELEASE_VERSION  :000.00000000.0\n");
#endif

#ifdef Version_NUMBER
	deb_data("        API_RX_RELEASE_VERSION  : %X.%X.%X\n", Version_NUMBER, Version_DATE, Version_BUILD);
#else
	deb_data("        API_RX_RELEASE_VERSION  :000.00000000.0\n");
#endif

	//************* Set Device init Info *************//
	PDC->bEnterSuspend = false;
    	PDC->bSurpriseRemoval = false;
    	PDC->bDevNotResp = false;
    	PDC->bSelectiveSuspend = false; 
	PDC->bTunerPowerOff = false;

	if (bBoot)
	{
		PDC->bSupportSelSuspend = false;
		PDC->demodulator.userData = (Handle)PDC;
		PDC->architecture=Architecture_DCA;
		PDC->modulator.frequency = 666000;
		PDC->modulator.bandwidth = 8000;
		PDC->bIrTblDownload = false;
		PDC->fc[0].tunerinfo.TunerId = 0;
		PDC->fc[1].tunerinfo.TunerId = 0;
		PDC->bDualTs=false;	
        	PDC->FilterCnt = 0;
		PDC->StreamType = StreamType_DVBT_DATAGRAM;
		PDC->UsbCtrlTimeOut = 1;
	}
	else {
        	PDC->UsbCtrlTimeOut = 5;
    	}//bBoot


#ifdef AFA_USB_DEVICE 	
	if (bBoot) {
		//************* Set USB Info *************//
		PDC->MaxPacketSize = 0x0200; //default USB2.0
		PDC->UsbMode = (PDC->MaxPacketSize == 0x200)?0x0200:0x0110;  
		deb_data("USB mode= 0x%x\n", PDC->UsbMode);

		PDC->TsPacketCount = (PDC->UsbMode == 0x200)?TS_PACKET_COUNT_HI:TS_PACKET_COUNT_FU;
		PDC->TsFrames = (PDC->UsbMode == 0x200)?TS_FRAMES_HI:TS_FRAMES_FU;
		PDC->TsFrameSize = TS_PACKET_SIZE*PDC->TsPacketCount;
		PDC->TsFrameSizeDw = PDC->TsFrameSize/4;
	}
	PDC->bEP12Error = false;
    	PDC->bEP45Error = false; 
    	PDC->ForceWrite = false;    
    	PDC->ulActiveFilter = 0;
#else
    	PDC->bSupportSuspend = false; 
#endif//AFA_USB_DEVICE
	
#ifdef AFA_USB_DEVICE
	if(bBoot)
    	{
		//patch for eeepc
#if IT9133Rx        
        	error = DL_SetBusTuner (PDC, Bus_USB, 0x38);
#else
        	error = DL_SetBusTuner (PDC, Bus_USB, 0x70);
#endif        	
        	if (error)
        	{
            		deb_data("First DL_SetBusTuner fail : 0x%lx\n",error );
			errcount++;
            		goto Exit; 
        	}

        	error =DL_GetEEPROMConfig(PDC);
        	if (error)
        	{
            		deb_data("DL_GetEEPROMConfig fail : 0x%lx\n", error);
			errcount++;
            		goto Exit;
        	}
	}//bBoot
	
	error = DL_SetBusTuner(PDC, Bus_USB, PDC->fc[0].tunerinfo.TunerId);
	
    	if (error)
    	{
        	deb_data("DL_SetBusTuner fail!\n");
		errcount++;
        	goto Exit;
    	}

	
	
	
	if(PDC->modulator.booted) //warm-boot/(S1)
	{
		error = DL_TunerWakeup(PDC);
	}
	if(error) deb_data("DL_NIMReset or DL_NIMSuspend or DL_TunerWakeup fail!\n"); 

	error = DL_Initialize(PDC);
	if (error) {
		deb_data("DL_Initialize fail! 0x%lx\n", error);
		errcount++;
		goto Exit;
	}
	
	if (PDC->bIrTblDownload) 
    	{
        	error = DL_IrTblDownload(PDC);
       	 	if (error) {deb_data("DL_IrTblDownload fail");errcount++;}
    	}

	
	deb_data("	%s success \n",__FUNCTION__);

	/*AirHD need to init some regs only for ep6-ep4 loop back*/

	error = IT9510_writeRegister((IT9510INFO*) &PDC->modulator, Processor_OFDM, 0xF7C6, 0x1);
	if(error)	deb_data( "AirHD Reg Write fail!\n");
	else deb_data( "AirHD Reg Write ok!\n");
	
	DL_SetDCCalibrationTable(PDC);

Exit:
#endif //AFA_USB_DEVICE
	
	if(errcount)
        deb_data( "[Device_init] Error %d\n", errcount);
	return (error);
}
#else
DWORD Device_init(struct usb_device *udev, PDEVICE_CONTEXT PDC, Bool bBoot)
{
	DWORD error = Error_NO_ERROR;
	int errcount=0;

	PDC->modulator.userData = (void *)udev;
	dev_set_drvdata(&udev->dev, PDC);

	deb_data("- Enter %s Function -\n",__FUNCTION__);

// define in it951x-core.h
#ifdef QuantaMID
	deb_data("    === AfaDTV on Quanta  ===\n");
#endif
#ifdef EEEPC
	deb_data("    === AfaDTV on EEEPC ===\n");
#endif

#ifdef DRIVER_RELEASE_VERSION
        deb_data("        DRIVER_RELEASE_VERSION  : %s\n", DRIVER_RELEASE_VERSION);
#else
	deb_data("        DRIVER_RELEASE_VERSION  : v0.0-0\n");
#endif

#ifdef __IT9510FIRMWARE_H__
	deb_data("        EAGLEII_FW_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", IT9510_DVB_LL_VERSION1, IT9510_DVB_LL_VERSION2, IT9510_DVB_LL_VERSION3, IT9510_DVB_LL_VERSION4);
	deb_data("        EAGLEII_FW_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", IT9510_DVB_OFDM_VERSION1, IT9510_DVB_OFDM_VERSION2, IT9510_DVB_OFDM_VERSION3, IT9510_DVB_OFDM_VERSION4);	
#else
	deb_data("        EAGLEII_FW_RELEASE_LINK_VERSION: v0_0_0_0\n");	
	deb_data("        EAGLEII_FW_RELEASE_OFDM_VERSION: v0_0_0_0\n");		
#endif

#ifdef __FIRMWARE_H__
	#if IT9133Rx
		deb_data("        OMEGA_FW_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", LL_VERSION1, LL_VERSION2, LL_VERSION3, LL_VERSION4);
		deb_data("        OMEGA_FW_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", OFDM_VERSION1, OFDM_VERSION2, OFDM_VERSION3, OFDM_VERSION4);	
	#else
		deb_data("        SAMBA_FW_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", DVB_LL_VERSION1, DVB_LL_VERSION2, DVB_LL_VERSION3, DVB_LL_VERSION4);
		deb_data("        SAMBA_FW_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", DVB_OFDM_VERSION1, DVB_OFDM_VERSION2, DVB_OFDM_VERSION3, DVB_OFDM_VERSION4);	
	#endif
#else
	deb_data("        RX_FW_RELEASE_LINK_VERSION: v0_0_0_0\n");	
	deb_data("        RX_FW_RELEASE_OFDM_VERSION: v0_0_0_0\n");		
#endif

#if IT9133Rx
	#ifdef __FIRMWAREV2_H__
		if(RX_LNA_TUNER_ID_SUPPORT_TYPE == OMEGA_LNA_Config_5) {		/* Support Decryption */
			deb_data("        OMEGA_FW_V2I_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", DVB_V2I_LL_VERSION1, DVB_V2I_LL_VERSION2, DVB_V2I_LL_VERSION3, DVB_V2I_LL_VERSION4);
			deb_data("        OMEGA_FW_V2I_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", DVB_V2I_OFDM_VERSION1, DVB_V2I_OFDM_VERSION2, DVB_V2I_OFDM_VERSION3, DVB_V2I_OFDM_VERSION4);				
		} else {
			deb_data("        OMEGA_FW_V2_RELEASE_LINK_VERSION: %d.%d.%d.%d\n", DVB_V2_LL_VERSION1, DVB_V2_LL_VERSION2, DVB_V2_LL_VERSION3, DVB_V2_LL_VERSION4);
			deb_data("        OMEGA_FW_V2_RELEASE_OFDM_VERSION: %d.%d.%d.%d\n", DVB_V2_OFDM_VERSION1, DVB_V2_OFDM_VERSION2, DVB_V2_OFDM_VERSION3, DVB_V2_OFDM_VERSION4);	
		}
	#else
		deb_data("        RX_FW_V2_RELEASE_LINK_VERSION: v0_0_0_0\n");	
		deb_data("        RX_FW_V2_RELEASE_OFDM_VERSION: v0_0_0_0\n");		
	#endif
#endif

#ifdef IT9510_Version_NUMBER 
	deb_data("        API_TX_RELEASE_VERSION  : %X.%X.%X\n", IT9510_Version_NUMBER, IT9510_Version_DATE, IT9510_Version_BUILD);
#else
	deb_data("        API_TX_RELEASE_VERSION  :000.00000000.0\n");
#endif

#ifdef Version_NUMBER
	deb_data("        API_RX_RELEASE_VERSION  : %X.%X.%X\n", Version_NUMBER, Version_DATE, Version_BUILD);
#else
	deb_data("        API_RX_RELEASE_VERSION  :000.00000000.0\n");
#endif

	//************* Set Device init Info *************//
	PDC->bEnterSuspend = false;
    	PDC->bSurpriseRemoval = false;
    	PDC->bDevNotResp = false;
    	PDC->bSelectiveSuspend = false; 
	PDC->bTunerPowerOff = false;

	if (bBoot)
	{
		PDC->bSupportSelSuspend = false;
		PDC->demodulator.userData = (Handle)PDC;
		PDC->architecture=Architecture_DCA;
		PDC->modulator.frequency = 666000;
		PDC->modulator.bandwidth = 8000;
		PDC->bIrTblDownload = false;
		PDC->fc[0].tunerinfo.TunerId = 0;
		PDC->fc[1].tunerinfo.TunerId = 0;
		PDC->bDualTs=false;	
        	PDC->FilterCnt = 0;
		PDC->StreamType = StreamType_DVBT_DATAGRAM;
		PDC->UsbCtrlTimeOut = 1;
	}
	else {
        	PDC->UsbCtrlTimeOut = 5;
    	}//bBoot


#ifdef AFA_USB_DEVICE 	
	if (bBoot) {
		//************* Set USB Info *************//
		PDC->MaxPacketSize = 0x0200; //default USB2.0
		PDC->UsbMode = (PDC->MaxPacketSize == 0x200)?0x0200:0x0110;  
		deb_data("USB mode= 0x%x\n", PDC->UsbMode);

		PDC->TsPacketCount = (PDC->UsbMode == 0x200)?TS_PACKET_COUNT_HI:TS_PACKET_COUNT_FU;
		PDC->TsFrames = (PDC->UsbMode == 0x200)?TS_FRAMES_HI:TS_FRAMES_FU;
		PDC->TsFrameSize = TS_PACKET_SIZE*PDC->TsPacketCount;
		PDC->TsFrameSizeDw = PDC->TsFrameSize/4;
	}
	PDC->bEP12Error = false;
    	PDC->bEP45Error = false; 
    	PDC->ForceWrite = false;    
    	PDC->ulActiveFilter = 0;
#else
    	PDC->bSupportSuspend = false; 
#endif//AFA_USB_DEVICE
	
#ifdef AFA_USB_DEVICE

	if(bBoot) {
		error =DL_GetEEPROMConfig(PDC);
		if (error) {
			deb_data("DL_GetEEPROMConfig fail : 0x%lx\n", error);
			errcount++;
			goto Exit;
		}
	}//bBoot

	
	error = DL_Initialize(PDC);
	if (error) {
		deb_data("DL_Initialize fail! 0x%lx\n", error);
		errcount++;
		goto Exit;
	}
	
	if (PDC->bIrTblDownload) 
    	{
        	error = DL_IrTblDownload(PDC);
       	 	if (error) {deb_data("DL_IrTblDownload fail");errcount++;}
    	}

	
	deb_data("	%s success \n",__FUNCTION__);

	/*AirHD need to init some regs only for ep6-ep4 loop back*/

	error = IT9510_writeRegister((IT9510INFO*) &PDC->modulator, Processor_OFDM, 0xF7C6, 0x1);
	if(error)	deb_data( "AirHD Reg Write fail!\n");
	else deb_data( "AirHD Reg Write ok!\n");
	
	DL_SetDCCalibrationTable(PDC);

Exit:
#endif //AFA_USB_DEVICE
	
	if(errcount)
        deb_data( "[Device_init] Error %d\n", errcount);
	return (error);
}
#endif


