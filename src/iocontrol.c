/**
 * Copyright (c) 2006-2016 ITEtech Corporation. All rights reserved.
 *
 * Module Name:
 *     iocontrol.cpp
 *
 * Abstract:
    Demodulator and Modulator IOCTL Query and Set functions
 */


#include "it951x-core.h"

/*****************************************************************************
*
*  Function:   DemodIOCTLFun
*
*  Arguments:  handle             - The handle of demodulator or modulator.
*              IOCTLCode               - Device IO control code
*              pIOBuffer               - buffer containing data for the IOCTL
*
*  Returns:    Error_NO_ERROR: successful, non-zero error code otherwise.
*
*  Notes:
*
*****************************************************************************/
/* IRQL:DISPATCH_LEVEL */
DWORD DemodIOCTLFun(
    void *       handle,
    Dword        IOCTLCode,
    unsigned long       pIOBuffer
    )
{
    Dword error = Error_NO_ERROR;

    switch (IOCTLCode)
    {
		case IOCTL_ITE_MOD_ADJUSTOUTPUTGAIN: 
		{
			TxSetGainRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxSetGainRequest)))
				return -EFAULT;

			if(((IT9510INFO*) handle)->isExtLo){		//	Use ADRF6755.
				if(Request.GainValue > -48 && Request.GainValue <= 0) {
					printk("ADRF6755 Request.GainValue: %d\n", (Byte) abs(Request.GainValue) );
					Request.error = IT9510User_adjustADRF6755Gain((IT9510INFO*) handle, (Byte) abs(Request.GainValue));
				} else {
					printk("ADRF6755 adjust gain out of range(%d)\n", Request.GainValue);
				}
			} else {
				printk("IT9510INFO Request.GainValue: %d\n", Request.GainValue);
				Request.error = IT9510_adjustOutputGain((IT9510INFO*) handle, &Request.GainValue);
			}
			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxSetGainRequest)))
				return -EFAULT;
				
			break;
		}
		case IOCTL_ITE_MOD_ENABLETXMODE:
		{
			TxModeRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxModeRequest)))
				return -EFAULT;

			Request.error = IT9510_setTxModeEnable((IT9510INFO*) handle, Request.OnOff);
			deb_data("IT951x TxMode RF %s\n", Request.OnOff?"ON":"OFF");	
			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxModeRequest)))
				return -EFAULT;
			break;
		}
		case IOCTL_ITE_MOD_SETMODULE:
		{
			ChannelModulation temp;
			TxSetModuleRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxSetModuleRequest)))
				return -EFAULT;

			temp.constellation = Request.constellation;
			temp.highCodeRate = Request.highCodeRate;
			temp.interval = Request.interval;
			temp.transmissionMode = Request.transmissionMode;
			Request.error = IT9510_setTXChannelModulation((IT9510INFO*) handle, &temp);
			Request.error = IT9510_setTxModeEnable((IT9510INFO*) handle, 1);
			deb_data("IT951x TxMode RF ON\n");
			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxSetModuleRequest)))
				return -EFAULT;
			
			break;
        }
        case IOCTL_ITE_MOD_ADDPIDAT:
        {
			TxAddPidAtRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxAddPidAtRequest)))
				return -EFAULT;

			Request.error = IT9510_addPidToFilter((IT9510INFO*) handle, Request.index, Request.pid);
						
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxAddPidAtRequest)))
				return -EFAULT;
    		break;
        }
		case IOCTL_ITE_MOD_ACQUIRECHANNEL:
		{
			TxAcquireChannelRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxAcquireChannelRequest)))
				return -EFAULT;
	
			Request.error = IT9510_acquireTxChannel((IT9510INFO*) handle, Request.bandwidth, Request.frequency);

			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxAcquireChannelRequest)))
				return -EFAULT;

			break;
        }
        case IOCTL_ITE_MOD_RESETPID:
        {
			TxResetPidRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxResetPidRequest)))
				return -EFAULT;
			
            Request.error = IT9510_resetPidFilter((IT9510INFO*) handle);
			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxResetPidRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_MOD_CONTROLPIDFILTER:
        {
			TxControlPidFilterRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxControlPidFilterRequest)))
				return -EFAULT;

			Request.error = IT9510_controlPidFilter((IT9510INFO*) handle, Request.control, Request.enable);
						
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxControlPidFilterRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_MOD_CONTROLPOWERSAVING:
        {
			TxControlPowerSavingRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxControlPowerSavingRequest)))
				return -EFAULT;

			Request.error = IT9510_setTxModeEnable((IT9510INFO*) handle, Request.control);
			Request.error = IT9510_controlPowerSaving((IT9510INFO*) handle, Request.control);
			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxControlPowerSavingRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_MOD_GETDRIVERINFO:
        {
			TxModDriverInfo Request;
			DWORD dwFWVerionLink = 0;
			DWORD dwFWVerionOFDM = 0;

			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxModDriverInfo)))
				return -EFAULT;

            strcpy((char *)Request.DriverVerion, DRIVER_RELEASE_VERSION);
			sprintf((char *)Request.APIVerion, "%X.%X.%X.%X", (BYTE)(IT9510_Version_NUMBER>>8), (BYTE)(IT9510_Version_NUMBER), IT9510_Version_DATE, IT9510_Version_BUILD);

			Request.error = IT9510_getFirmwareVersion ((IT9510INFO*) handle, Processor_LINK, &dwFWVerionLink);
			sprintf((char *)Request.FWVerionLink, "%d.%d.%d.%d", (BYTE)(dwFWVerionLink>>24), (BYTE)(dwFWVerionLink>>16), (BYTE)(dwFWVerionLink>>8), (BYTE)dwFWVerionLink);
			deb_data("Modulator_getFirmwareVersion Processor_LINK %s\n", (char *)Request.FWVerionLink);
 
			Request.error = IT9510_getFirmwareVersion ((IT9510INFO*) handle, Processor_OFDM, &dwFWVerionOFDM);
			sprintf((char *)Request.FWVerionOFDM, "%d.%d.%d.%d", (BYTE)(dwFWVerionOFDM>>24), (BYTE)(dwFWVerionOFDM>>16), (BYTE)(dwFWVerionOFDM>>8), (BYTE)dwFWVerionOFDM);
			deb_data("Modulator_getFirmwareVersion Processor_OFDM %s\n", (char *)Request.FWVerionOFDM);

			strcpy((char *)Request.Company, "ITEtech");
			strcpy((char *)Request.SupportHWInfo, "EagleII DVBT & ISDB-T");

			Request.error = Error_NO_ERROR;
			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxModDriverInfo)))
				return -EFAULT;
				
    		break;
        }
        case IOCTL_ITE_MOD_WRITEREGISTERS:
        {
			TxWriteRegistersRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxWriteRegistersRequest)))
				return -EFAULT;

            Request.error = IT9510_writeRegisters((IT9510INFO*) handle, Request.processor, Request.registerAddress, Request.bufferLength, Request.buffer);
			           			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxWriteRegistersRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_MOD_WRITEEEPROMVALUES:
        {
			TxWriteEepromValuesRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxWriteEepromValuesRequest)))
				return -EFAULT;

			Request.error = IT9510_writeEepromValues((IT9510INFO*) handle, Request.registerAddress, Request.bufferLength, Request.buffer);
						                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxWriteEepromValuesRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_MOD_READREGISTERS:
        {
			TxReadRegistersRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxReadRegistersRequest)))
				return -EFAULT;

			Request.error = IT9510_readRegisters((IT9510INFO*) handle, Request.processor, Request.registerAddress, Request.bufferLength, Request.buffer);
															                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxReadRegistersRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_MOD_READEEPROMVALUES:
        {
			TxReadEepromValuesRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxReadEepromValuesRequest)))
				return -EFAULT;

			Request.error = IT9510_readEepromValues((IT9510INFO*) handle, Request.registerAddress, Request.bufferLength, Request.buffer);
																		                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxReadEepromValuesRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_MOD_GETGAINRANGE:
        {
			TxGetGainRangeRequest Request;

			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxGetGainRangeRequest)))
				return -EFAULT;
				
			if(((IT9510INFO*) handle)->isExtLo) {
				Request.maxGain = 0;
				Request.minGain	= -47;
				Request.error = Error_NO_ERROR;
			} else {
				Request.error = IT9510_getGainRange ((IT9510INFO*) handle, (Dword) Request.frequency, (Word) Request.bandwidth, &Request.maxGain, &Request.minGain);
			}
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxGetGainRangeRequest)))
				return -EFAULT;

			break;
		}
        case IOCTL_ITE_MOD_GETTPS:
        {
			TxGetTPSRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxGetTPSRequest)))
				return -EFAULT;

			Request.error = IT9510_getTPS((IT9510INFO*) handle, &Request.tps);
																					                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxGetTPSRequest)))
				return -EFAULT;
			break;
		}
        case IOCTL_ITE_MOD_SETTPS:
        {
			TxSetTPSRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxSetTPSRequest)))
				return -EFAULT;

			Request.error = IT9510_setTPS((IT9510INFO*) handle, Request.tps, Request.actualInfo);
																								                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxSetTPSRequest)))
				return -EFAULT;
			break;
		}
        case IOCTL_ITE_MOD_GETOUTPUTGAIN:
        {
			TxGetOutputGainRequest Request;
			Byte tmp_gain;
			
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxGetOutputGainRequest)))
				return -EFAULT;

			if(((IT9510INFO*) handle)->isExtLo){
				Request.error = IT9510User_getADRF6755Gain((IT9510INFO*) handle, &tmp_gain);
				Request.gain = 0-tmp_gain;
			} else {
				Request.error = IT9510_getOutputGain ((IT9510INFO*) handle, &Request.gain);
			}
			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxGetOutputGainRequest)))
				return -EFAULT;
			break;
		}
        case IOCTL_ITE_MOD_SENDHWPSITABLE:
        {
			TxSendHwPSITableRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxSendHwPSITableRequest)))
				return -EFAULT;

			Request.error = IT9510_sendHwPSITable((IT9510INFO*) handle, (Byte*) Request.pbufferAddr);
																														                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxSendHwPSITableRequest)))
				return -EFAULT;
			break;
		}
        case IOCTL_ITE_MOD_ACCESSFWPSITABLE:
        {
			TxAccessFwPSITableRequest Request;
			Byte* pbufferAddrUser;
			Byte pbufferAddrKernel[188];
			
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxAccessFwPSITableRequest)))
				return -EFAULT;

			pbufferAddrUser = (Byte*) Request.pbufferAddr;
			if (copy_from_user(pbufferAddrKernel, pbufferAddrUser, 188)) {
				return -EFAULT;
			}
			Request.error = IT9510_accessFwPSITable ((IT9510INFO*) handle, Request.psiTableIndex, (Byte*) pbufferAddrKernel);

			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxAccessFwPSITableRequest)))
				return -EFAULT;

			break;
		}		
        case IOCTL_ITE_MOD_SETFWPSITABLETIMER:
        {
			TxSetFwPSITableTimerRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxSetFwPSITableTimerRequest)))
				return -EFAULT;

			Request.error = IT9510_setFwPSITableTimer((IT9510INFO*) handle, Request.psiTableIndex, Request.timer);
																																	                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxSetFwPSITableTimerRequest)))
				return -EFAULT;
			break;
		}
		case IOCTL_ITE_MOD_SETDCCALIBRATIONVALUE:
		{
			TxSetDCCalibrationValueRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxSetDCCalibrationValueRequest)))
				return -EFAULT;

			Request.error = IT9510_setDCCalibrationValue((IT9510INFO*) handle, Request.dc_i, Request.dc_q);
			deb_data("Set DC Calibration dc_i %d, dc_q %d\n", Request.dc_i, Request.dc_q);
																														                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxSetDCCalibrationValueRequest)))
				return -EFAULT;
			break;
		}
		case IOCTL_ITE_MOD_GETCHIPTYPE:
		{
			TxGetChipTypeRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxGetChipTypeRequest)))
				return -EFAULT;

			Request.error = IT9510_getChipType((IT9510INFO*) handle, &Request.chipType);

			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxGetChipTypeRequest)))
				return -EFAULT;
			break;
		}		
        case IOCTL_ITE_MOD_SETISDBTCHANNELMODULATION:
        {
			TXSetISDBTChannelModulationRequest Request;
			ISDBTModulation* isdbtModulationUser;
			ISDBTModulation isdbtModulationKernel;
			
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TXSetISDBTChannelModulationRequest)))
				return -EFAULT;

			isdbtModulationUser = (ISDBTModulation*) Request.isdbtModulationAddr;
			if (copy_from_user(&isdbtModulationKernel, isdbtModulationUser, sizeof(ISDBTModulation))) {
				return -EFAULT;
			}
			Request.error = IT9510_setISDBTChannelModulation ((IT9510INFO*) handle, isdbtModulationKernel);
			Request.error = IT9510_setTxModeEnable((IT9510INFO*) handle, 1);
			deb_data("IT951x TxMode RF ON\n");
																																	                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TXSetISDBTChannelModulationRequest)))
				return -EFAULT;
			break;
		}		
        case IOCTL_ITE_MOD_SETTMCCINFO:
        {
			TXSetTMCCInfoRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TXSetTMCCInfoRequest)))
				return -EFAULT;

			Request.error = IT9510_setTMCCInfo((IT9510INFO*) handle, Request.TmccInfo, Request.actualInfo);
																																	                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TXSetTMCCInfoRequest)))
				return -EFAULT;
			break;
		}		
        case IOCTL_ITE_MOD_GETTMCCINFO:
        {
			TXGetTMCCInfoRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TXGetTMCCInfoRequest)))
				return -EFAULT;

			Request.error = IT9510_getTMCCInfo((IT9510INFO*) handle, &Request.TmccInfo);
																																	                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TXGetTMCCInfoRequest)))
				return -EFAULT;
			break;
		}		
        case IOCTL_ITE_MOD_GETTSINPUTBITRATE:
        {
			TXGetTSinputBitRateRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TXGetTSinputBitRateRequest)))
				return -EFAULT;

			Request.error = IT9510_getTSinputBitRate((IT9510INFO*) handle, &Request.BitRate_Kbps);
																																				                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TXGetTSinputBitRateRequest)))
				return -EFAULT;
			break;
		}
        case IOCTL_ITE_MOD_ADDPIDTOISDBTPIDFILTER:
        {
			TXAddPidToISDBTPidFilterRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TXAddPidToISDBTPidFilterRequest)))
				return -EFAULT;

			Request.error = IT9510_addPidToISDBTPidFilter((IT9510INFO*) handle, Request.index, Request.pid, Request.layer);
																																							                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TXAddPidToISDBTPidFilterRequest)))
				return -EFAULT;
			break;
		}
		case IOCTL_ITE_MOD_SETPCRMODE:
		{
			TxSetPcrModeRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxSetPcrModeRequest)))
				return -EFAULT;

			Request.error = IT9510_setPcrMode((IT9510INFO*) handle, Request.mode);
			deb_data("Enable PCR Mode %d\n", Request.mode);
																																										                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxSetPcrModeRequest)))
				return -EFAULT;
			break;
		}
		case IOCTL_ITE_MOD_GETDTVMODE:
		{
			PTxGetDTVModeRequest pRequest = (PTxGetDTVModeRequest) pIOBuffer;		
			Byte temp = 0;
			
			error = IT9510_readRegister ((IT9510INFO*) handle, Processor_LINK, 0x4979, &temp);// has eeprom ?
			if((temp == 1) && (error == ModulatorError_NO_ERROR)){  // eeprom, read value
				pRequest->error = IT9510_readRegisters ((IT9510INFO*) handle, Processor_LINK, 0x49C5, 1, &pRequest->DTVMode);	
			} else {    // no eeprom, return Error
				pRequest->DTVMode = 0;
				pRequest->error = ModulatorError_USB_READ_FAIL;
			}
			break;
		}
		case IOCTL_ITE_MOD_GETFREQUENCYINDEX:
		{
			TxGetFrequencyIndexRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxGetFrequencyIndexRequest)))
				return -EFAULT;

			Request.error = IT9510User_getChannelIndex((IT9510INFO*) handle, &(Request.frequencyindex));
																																							                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxGetFrequencyIndexRequest)))
				return -EFAULT;
			break;
		}
		case IOCTL_ITE_MOD_ENABLETPSENCRYPTION:
		{
			TxEnableTpsEncryptionRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxEnableTpsEncryptionRequest)))
				return -EFAULT;
			
			Request.error = IT9510_enableTpsEncryption((IT9510INFO*) handle, (Dword) Request.key);
																																										                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxEnableTpsEncryptionRequest)))
				return -EFAULT;
			break;
		}
		case IOCTL_ITE_MOD_DISABLETPSENCRYPTION:
		{
			TxDisableTpsEncryptionRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxDisableTpsEncryptionRequest)))
				return -EFAULT;
			
			Request.error = IT9510_disableTpsEncryption ((IT9510INFO*) handle);
																																							                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxDisableTpsEncryptionRequest)))
				return -EFAULT;
			break;
		}
        case IOCTL_ITE_BB_IT9560_GETSTATISTIC:
        {
			IT9560GetStatisticRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(IT9560GetStatisticRequest)))
				return -EFAULT;

			Request.error = IT9510_getIT9560FwVersion((IT9510INFO*) handle, 0xa8, Request.FwVersion);
			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(IT9560GetStatisticRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_BB_IT9560_WRITEEEPROMVALUES:
        {
			IT9560WriteEepromValuesRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(IT9560WriteEepromValuesRequest)))
				return -EFAULT;
			
			Request.error = IT9510_writeIT9560EEPROM((IT9510INFO*) handle, Request.slaveAddress, Request.startAddressOffset, Request.buffer, Request.writeSize);

			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(IT9560WriteEepromValuesRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_BB_IT9560_READEEPROMVALUES:
        {
			IT9560ReadEepromValuesRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(IT9560ReadEepromValuesRequest)))
				return -EFAULT;

			Request.error = IT9510_readIT9560EEPROM((IT9510INFO*) handle, Request.slaveAddress, Request.startAddressOffset, Request.buffer, Request.readSize);
			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(IT9560ReadEepromValuesRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_BB_IT9560_WRITEREGISTERS:
        {
			IT9560WriteRegistersRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(IT9560WriteRegistersRequest)))
				return -EFAULT;

            Request.error = IT9510_writeRegisters((IT9510INFO*) handle, Request.processor, Request.registerAddress, Request.bufferLength, Request.buffer);
			           			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(IT9560WriteRegistersRequest)))
				return -EFAULT;
    		break;
        }
        case IOCTL_ITE_BB_IT9560_READREGISTERS:
        {
			IT9560ReadRegistersRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(IT9560ReadRegistersRequest)))
				return -EFAULT;

			Request.error = IT9510_readRegisters((IT9510INFO*) handle, Request.processor, Request.registerAddress, Request.bufferLength, Request.buffer);
															                       			
			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(IT9560ReadRegistersRequest)))
				return -EFAULT;
    		break;
        }
		case IOCTL_ITE_MOD_ACQUIRECHANNELDUAL:
		{
			TxAcquireChannelDualRequest Request;
			if (copy_from_user((void *)&Request, (void *)pIOBuffer, sizeof(TxAcquireChannelDualRequest)))
				return -EFAULT;
	
			Request.error = IT9510_acquireTxChannelDual((IT9510INFO*) handle, Request.bandwidth, Request.frequency1, Request.frequency2);

			if (copy_to_user((void *)pIOBuffer, (void *)&Request, sizeof(TxAcquireChannelDualRequest)))
				return -EFAULT;

			break;
        }
        default:
        {
            deb_data("NOT SUPPORTED IOCONTROL! 0x%08x\n", (unsigned int)IOCTLCode);
            return ENOTTY;
        }
    }
    
    return error;
}
