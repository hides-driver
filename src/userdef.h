#ifndef _USERDEF_H_
#define _USERDEF_H_


typedef     int             INT;       // 4 bytes

#ifdef NULL
#undef NULL
#endif

#ifdef IN
#undef IN
#endif

#ifdef OUT
#undef OUT
#endif

#define NULL    0

#endif // _USERDEF_H_

